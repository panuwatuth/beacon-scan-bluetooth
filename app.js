const BeaconScanner = require('node-beacon-scanner')
const scanner = new BeaconScanner()
var noble = require('noble');
const moment = require('moment')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var socketServer = io.of('/scanner'); 
var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var LED = new Gpio(13, 'out'); //use GPIO pin 4, and specify that it is output
// var blinkInterval = setInterval(blinkLED, 250); //run the blinkLED function every 250ms 

var blinkTimeout = undefined
function blinkLED() { //function to start blinking
  // if (LED.readSync() === 0) { //check the pin state, if the state is 0 (or off)
    LED.writeSync(1); //set pin state to 1 (turn LED on)
  // } else {
  //   LED.writeSync(0); //set pin state to 0 (turn LED off)
  // }
}

function endBlink() { //function to stop blinking
  // clearInterval(blinkInterval); // Stop blink intervals
  LED.writeSync(0); // Turn LED off
  // LED.unexport(); // Unexport GPIO to free resources
}

// setTimeout(endBlink, 5000); //stop blinking after 5 seconds


socketServer.on('connection', function(socket) {
    console.log('Scanner Connected');
    // var temp , diff , weightRssi , operator
    // 91b01ccd77a9c083
    scanner.onadvertisement = (ad) => {
      if(ad.beaconType == 'estimoteTelemetry' && 
         ad.estimoteTelemetry.shortIdentifier == 'b2949de510ad184e'){
          // console.log('uuid : '+ ad.iBeacon.uuid + ' major : ' + ad.iBeacon.major + ' minor : ' + ad.iBeacon.minor + ' time : ' + moment().format('HH:mm:ss'))
          // socket.emit('send-distance-rssi', {"id":ad.id ,"mac": ad.iBeacon.uuid, "rssi":ad.rssi})
          console.log(ad)
        }
      if(ad.iBeacon){
          // if(ad.iBeacon.uuid == "FDA50693-A4E2-4FB1-AFCF-C6EB07647825" && ad.iBeacon.major == "4" && ad.iBeacon.minor == "47" && ad.rssi == '-59'){
          // console.log('rssi :' + ad.rssi + ' txPower : ' + ad.iBeacon.txPower + ' time : ' + moment().format('HH:mm:ss'))
          //   socket.emit('send-distance-rssi', {"id":ad.id ,"mac": ad.iBeacon.uuid, "rssi":ad.rssi})
          //   if(ad.rssi > -77){
          //     blinkLED()
          //   }else{
          //     if(LED.readSync() === 1){
          //       endBlink()
          //     }
          //   }
          // }

          // if(ad.iBeacon.uuid == "6E732E63-6F2E-7468-2D69-626561636F6E" && ad.iBeacon.major == "1" && ad.iBeacon.minor == "1002"){
          //     setNotificationAndSendData(ad.rssi , ad.id , ad.iBeacon.uuid , socket)
          //     console.log(ad)
          // }
          // if(ad.iBeacon.uuid == "FDA50693-A4E2-4FB1-AFCF-C6EB07647825" && ad.iBeacon.major == "4" && ad.iBeacon.minor == "47"){
          //     // console.log('uuid : '+ ad.iBeacon.uuid + ' major : ' + ad.iBeacon.major + ' minor : ' + ad.iBeacon.minor + ' time : ' + moment().format('HH:mm:ss'))
          //     setNotificationAndSendData(ad.rssi , ad.id , ad.iBeacon.uuid , socket)
          //     console.log(ad)
          //   }  


          // if(ad.iBeacon.uuid == "B9407F30-F5F8-466E-AFF9-25556B57FE6D" && ad.iBeacon.major == "29641" && ad.iBeacon.minor == "28675"){
          //   // console.log('uuid : '+ ad.iBeacon.uuid + ' major : ' + ad.iBeacon.major + ' minor : ' + ad.iBeacon.minor + ' time : ' + moment().format('HH:mm:ss'))
          //   socket.emit('send-distance-rssi', {"id":ad.id ,"mac": ad.iBeacon.uuid, "rssi":ad.rssi})
          // }
          // if(ad.iBeacon.uuid == "B9407F30-F5F8-466E-AFF9-25556B57FE6D" && ad.iBeacon.major == "6741" && ad.iBeacon.minor == "61346"){
          //   socket.emit('send-distance-rssi', Buffer.from(JSON.stringify({"id":ad.id ,"mac": ad.iBeacon.uuid, "rssi":ad.rssi})))
          // }
          // Number 10
          // if(ad.iBeacon.uuid == "B9407F30-F5F8-466E-AFF9-25556B57FE6D" && ad.iBeacon.major == "8366" && ad.iBeacon.minor == "47921"){
          //   socket.emit('send-distance-rssi', Buffer.from(JSON.stringify({"id":ad.id ,"mac": ad.iBeacon.uuid, "rssi":ad.rssi})))
          // }
          // if(ad.iBeacon.uuid == "B9407F30-F5F8-466E-AFF9-25556B57FE6D" && ad.iBeacon.major == "27145" && ad.iBeacon.minor == "14396"){
          //   socket.emit('send-distance-rssi', Buffer.from(JSON.stringify({"id":ad.id ,"mac": ad.iBeacon.uuid, "rssi":ad.rssi})))
          // }
      }
    }
    socket.on('disconnect', function() {
        console.log('Scanner Disconnected');
        endBlink()
    });
});
var temp , diff , weightRssi , operator

function setNotificationAndSendData(rssi , id , uuid , socket){
    if(!temp){
      temp = rssi
      diff = temp
    }else{
      let total = Math.abs(temp - rssi) * -1
      operator = (temp - rssi) < 0 ? -1 : 1
      diff = total
    }

    if(diff < -9){                
      weightRssi = rssi + (7 * operator)
    }
    else if(diff >= -9 && diff <= -5){
      weightRssi = rssi + (6 * operator)
    }else{
      weightRssi = rssi
    }
    if(weightRssi > -90){
      clearTimeout(blinkTimeout)
      blinkLED()
      temp = weightRssi
      blinkTimeout = setTimeout(() => {
        endBlink()
      }, 3000);
    }else{
      if(LED.readSync() === 1){
        endBlink()
        temp = weightRssi
      }
    }
    socket.emit('send-distance-rssi', {"id": id ,"mac": uuid, "rssi":weightRssi})

    console.log("mac :" + uuid + " Weight Rssi : " + weightRssi + " Real Rssi : " + rssi)
}
http.listen(3000 , function(){
    console.log("listen on port 3000")
})

// Start scanning
scanner.startScan().then(() => {
  console.log('Started to scan.')  ;
}).catch((error) => {
  console.error(error);
});

process.on('exit', function (){
  endBlink()
  console.log('Goodbye!');
});